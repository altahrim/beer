#!/usr/bin/env bash
# @file BEER init file
# @brief This file must be included first in your project.
# Framework variables
declare -g B2R_ROOT_PATH
B2R_ROOT_PATH="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
declare -gr B2R_ROOT_PATH
declare -gA B2R_LOADED_MODULES=()

# @description Initialize framework
# Enforces best practices and declare global variables.
#
# @exitcode 0
function b2r:_init() {
  set -e
  set -u
  set -o noclobber
  shopt -s checkwinsize

  declare -g B2R_DEBUG=${B2R_DEBUG:-false}
  declare -g B2R_INTERNAL_DEBUG=${B2R_INTERNAL_DEBUG:-false}
  declare -g B2R_VERBOSE=${B2R_VERBOSE:-false}
  declare -g B2R_QUIET=${B2R_QUIET:-false}

  b2r:import output print
  b2r:print:debug 'Debug enabled.\n'
}

# @description Import a framework module.
# Note: be careful when you use framework in this function, it may be unavailable.
#
# @arg $1 string Name of module to import
# @arg $@ string Files to import in specified module (without sh)
# @exitcode 0 Module (already) loaded
# @exitcode 1 Module doesn't exist
# @exitcode 2 File specified doesn't exist
# @stderr Error if module doesn’t exits or file missing
function b2r:import() {
  declare -r MODULE="${1}"

  # Check if module exists
  if [[ ! -d "${B2R_ROOT_PATH}/src/${MODULE}" ]]; then
    declare -ra ERROR_MSG=("Module \"%s\" doesn’t exist.\\n" "${MODULE}")
    if b2r:import output print >& /dev/null; then
      b2r:print:error "${ERROR_MSG[@]}"
    else
      # shellcheck disable=SC2059
      printf "${ERROR_MSG[@]}"
    fi
    return 1;
  fi

  # If file list is empty, load complete module
  shift 1
  declare -a FILES=("${@}")
  if [[ -z "${FILES[*]}" ]]; then
    readarray -t FILES < <(find "${B2R_ROOT_PATH}/src/${MODULE}" -type f -name '*.sh' -print0 | xargs -0n10 -P4 basename -s .sh )
  fi

  # Load each specified file
  declare FILE_PATH
  for FILE in "${FILES[@]}"; do
    FILE_PATH="${MODULE}/${FILE}.sh"
    if [[ -n "${B2R_LOADED_MODULES[${FILE_PATH}]:-}" ]]; then
      continue
    fi
    if [[ ! -f "${B2R_ROOT_PATH}/src/${FILE_PATH}" ]]; then
      declare -ra ERROR_MSG=("File \"%s\" doesn’t exist in %s.\\n" "${FILE}" "${MODULE}")
      if b2r:import output print >& /dev/null; then
        b2r:print:error "${ERROR_MSG[@]}"
      else
        # shellcheck disable=SC2059
        printf "${ERROR_MSG[@]}"
      fi
      return 2;
    fi

    # shellcheck disable=SC1090
    B2R_LOADED_MODULES[${FILE_PATH}]=1

    # Load dependencies
    declare SRC=""
    SRC="$(cat "${B2R_ROOT_PATH}/src/${FILE_PATH}")"
    while read -r INCLUDE; do
      # shellcheck disable=SC2086
      b2r:import ${INCLUDE}
    done < <(<<< "${SRC}" grep "^#\+\s*@require\s" | sed 's/^#\+\s*@require\s//')

    # Load module
    #shellcheck disable=SC1090
    source <(echo "${SRC}")
  done
}

# Launch init
b2r:_init
