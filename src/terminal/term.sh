#!/usr/bin/env bash
# @file Terminal
# @brief Various information about current terminal.
# Dependencies

function b2r:term:cols() {
  echo "${COLUMNS}"
}

function b2r:term:lines() {

  echo "${LINES}"
}

function b2r:term:clear() {
  printf "%s[2J" "${B2R_ESC}"
  b2r:cursor:move 1 1
}

function b2r:term:stdin:is_tty() {
  TODO
}
function b2r:term:stdout:is_tty() {
  TODO
}
function b2r:term:stderr:is_tty() {
  TODO
}
