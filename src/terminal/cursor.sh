#!/usr/bin/env bash
# Dependencies
# @require output special_chars
# @require terminal term

# @description Move cursor to desired position.
#
# @arg $1 int Line number (if omitted, cursor will stay on same line).
# @arg $2 int Column number (if omitted, cursor will stay on same column).
# @stdout ANSI escape sequence.
# @stderr Warning if no argument provided.
function b2r:cursor:move() {
  declare LINE="${1:-}" COLUMN="${2:-}" MAX
  if [[ "${LINE}" -lt 0 ]]; then
    MAX="$(b2r:term:lines)"
    LINE=$(( MAX + LINE ))
  fi
  if [[ "${COLUMN}" -lt 0 ]]; then
    MAX="$(b2r:term:cols)"
    COLUMN=$(( MAX + COLUMN ))
  fi

  if [[ -z "${LINE}" ]]; then
    LINE="$(b2r:cursor:get_line)"
  fi
  if [[ -z "${COLUMN}" ]]; then
    COLUMN="$(b2r:cursor:get_column)"
  fi

  printf "%s[%i;%iH" "${B2R_ESC}" "${LINE}" "${COLUMN}"
}

function b2r:cursor:save() {
  printf "%s[s" "${B2R_ESC}"
}

function b2r:cursor:restore() {
  printf "%s[u" "${B2R_ESC}"
}

# @description Print cursor position
#
# @stdout <LineNumber>;<ColumnNumber>
function b2r:cursor:get_pos() {
  declare POSITION
  #shellcheck disable=SC2162
  read -sdR -p "${B2R_ESC}[6n" POSITION
  echo -n "${POSITION#*[}"
}

# @description Print cursor line number
#
# @stdout Line number
function b2r:cursor:get_line() {
  declare POS
  POS="$(b2r:cursor:get_pos)"
  echo "${POS%;*}"
}

# @description Print cursor column number
#
# @stdout Column number
function b2r:cursor:get_column() {
  declare POS
  POS="$(b2r:cursor:get_pos)"
  echo "${POS#*;}"
}
