#!/usr/bin/env bash

# Escape character for ANSI sequences (\e).
# shellcheck disable=SC2155
declare -gxr B2R_ESC=$(printf '\e')

# NUL character (\0).
# shellcheck disable=SC2155
declare -gxr B2R_NUL=$'\0'

# Unix linebreak (\n)
# shellcheck disable=SC2155
declare -gxr B2R_NL=$(printf '\n')
