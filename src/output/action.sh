#!/usr/bin/env bash
# @file Actions
# @brief Time commands and display result.
# Dependencies
# @require output print align
# @require datetime timer duration
# Module variables
declare -gi B2R_ACTION_COUNTER=0

function b2r:action:start() {
  b2r:print:_prefix '…' 25
  declare MSG="${1}\\n"
  shift
  b2r:print:important "${MSG}" "${@}"
  B2R_ACTION_COUNTER+=1
  b2r:timer:start "b2r_action_${B2R_ACTION_COUNTER}"
}

function b2r:action:_end() {
  declare -r SYMBOL="${1}"
  declare -r COLOR="${2:-0}"
  declare ELAPSED
  ELAPSED="$(b2r:timer:stop "b2r_action_${B2R_ACTION_COUNTER}")"
  ELAPSED="$(b2r:duration:float_to_human "${ELAPSED}" 2)"
  if [[ -z "${SYMBOL}" ]]; then
    b2r:align:right b2r:print:output "( %s )\n" "${ELAPSED}"
  else
    b2r:align:right b2r:print:output "( %s%s%s %s )\n" "$(b2r:color "${COLOR}")" "${SYMBOL}" "$(b2r:ansi:reset)" "${ELAPSED}"
  fi
}

function b2r:action:end() {
  b2r:action:_end ''
}

function b2r:action:success() {
  b2r:action:_end '✔' 40
}

function b2r:action:failure() {
  b2r:action:_end '✘' 196
}

function b2r:action:exec() {
  b2r:action:start "${1}"
  shift
  if "${@}"; then
    b2r:action:success
  else
    b2r:action:failure
  fi
}
