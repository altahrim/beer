#!/usr/bin/env bash
# Dependencies
# @require output special_chars

# @description Print ANSI escape sequence.
#
# @example
#   b2r:ansi:seq 38 5 180
#   echo \e[38;5;180m
#
# @arg $@ int Numbers for ANSI sequence.
# @stdout ANSI escape sequence.
# @stderr Warning if no argument provided.
function b2r:ansi:seq() {
  if [[ -z "${*}" ]]; then
    warning "No escape sequence provided."
  fi
  declare IFS=';'
  printf "%s[%sm" "${B2R_ESC}" "${*}"
}

function b2r:ansi:reset() {
  b2r:ansi:seq 0
}

function b2r:ansi:bold() {
  b2r:ansi:seq 1
}

function b2r:ansi:italic() {
  b2r:ansi:seq 3
}

function b2r:ansi:underline() {
  b2r:ansi:seq 4
}

function b2r:ansi:strike() {
  b2r:ansi:seq 4
}

function b2r:ansi:strip() {
  declare -r TEXT="${1}"
  ESC_PREFIX="$(printf '\033[')"
  # shellcheck disable=SC2001
  <<< "${TEXT}" sed "s/${ESC_PREFIX}[0-9;]\\+m//g"
}
