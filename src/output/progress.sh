#!/usr/bin/env bash
# @file Progress
# @brief Displays progress bars
# Dependencies
# @require output print
# @require terminal cursor term
# @require variable string

declare -gi B2R_PROGRESS_CURRENT=0
declare -gi B2R_PROGRESS_NB_STEPS=10
declare -gi B2R_PROGRESS_MAX_WIDTH

declare -gi B2R_PROGRESS_TYPE_NONE=0
declare -gi B2R_PROGRESS_TYPE_STEPS=1
declare -gi B2R_PROGRESS_TYPE_RATIO=2
declare -gi B2R_PROGRESS_TYPE_PERCENT=3
declare -gi B2R_PROGRESS_TYPE="${B2R_PROGRESS_TYPE_PERCENT}"

function b2r:progress:init() {
  B2R_PROGRESS_CURRENT=0
  B2R_PROGRESS_NB_STEPS="${1:-10}"
  b2r:progress:_compute_progress_width
  b2r:print:nl
  b2r:cursor:save
  b2r:progress:_display
}

function b2r:progress:next() {
  if [[ "$(( ++ B2R_PROGRESS_CURRENT ))" -lt "${B2R_PROGRESS_NB_STEPS}" ]]; then
    b2r:progress:_display
    return 0
  fi

  b2r:progress:success
  return 1
}

function b2r:progress:_display() {
  b2r:cursor:restore
  declare -i TERM_WIDTH AVAIL_WIDTH NB_SEGMENTS
  TERM_WIDTH="$(b2r:term:cols)"
  AVAIL_WIDTH="$((TERM_WIDTH - B2R_PROGRESS_MAX_WIDTH - 1))"
  NB_SEGMENTS="$((AVAIL_WIDTH * B2R_PROGRESS_CURRENT / B2R_PROGRESS_NB_STEPS))"

  declare PROGRESS=""
  case "${B2R_PROGRESS_TYPE}" in
    "${B2R_PROGRESS_TYPE_STEPS}")
      PROGRESS="${B2R_PROGRESS_CURRENT}"
      ;;
    "${B2R_PROGRESS_TYPE_RATIO}")
      PROGRESS="${B2R_PROGRESS_CURRENT}/${B2R_PROGRESS_NB_STEPS}"
      ;;
    "${B2R_PROGRESS_TYPE_PERCENT}")
      PROGRESS="$((100 * B2R_PROGRESS_CURRENT / B2R_PROGRESS_NB_STEPS))%"
      ;;
  esac

  case "${NB_SEGMENTS}" in
    0)
      b2r:print:output "%s▏%*s%s" "$(b2r:color:back 238)" "$((AVAIL_WIDTH - 1))" ""
      ;;
    "$((AVAIL_WIDTH))")
      b2r:print:output "%s▏%*s" "$(b2r:color:back 31)" "$((AVAIL_WIDTH - 1))" ""
      ;;
    *)
      b2r:print:output "%s▏%*s%s%*s" "$(b2r:color:back 31)" "$((NB_SEGMENTS - 1))" "" "$(b2r:color:back 238)" "$((AVAIL_WIDTH - NB_SEGMENTS))" ""
      ;;
  esac
  if [[ "${B2R_PROGRESS_MAX_WIDTH}" -gt 0 ]]; then
    b2r:print:output "%s▏%*s" "$(b2r:color:back 35)" "$((B2R_PROGRESS_MAX_WIDTH - 1))" "${PROGRESS}"
  fi
  b2r:print:output "▕"
  b2r:ansi:reset
}

function b2r:progress:_compute_progress_width() {
  case "${B2R_PROGRESS_TYPE}" in
    "${B2R_PROGRESS_TYPE_NONE}")
      B2R_PROGRESS_MAX_WIDTH=0
      ;;
    "${B2R_PROGRESS_TYPE_STEPS}")
      B2R_PROGRESS_MAX_WIDTH="${#B2R_PROGRESS_NB_STEPS}"
      ;;
    "${B2R_PROGRESS_TYPE_RATIO}")
      B2R_PROGRESS_MAX_WIDTH="${#B2R_PROGRESS_NB_STEPS}"
      B2R_PROGRESS_MAX_WIDTH="$((B2R_PROGRESS_MAX_WIDTH * 2 + 1))"
      ;;
    "${B2R_PROGRESS_TYPE_PERCENT}")
      B2R_PROGRESS_MAX_WIDTH=4
      ;;
  esac

  ## Include separators
  if [[ "${B2R_PROGRESS_MAX_WIDTH}" -gt 0 ]]; then
    B2R_PROGRESS_MAX_WIDTH="$((B2R_PROGRESS_MAX_WIDTH + 1))"
  fi
}

function b2r:progress:success() {
  B2R_PROGRESS_CURRENT="${B2R_PROGRESS_NB_STEPS}"
  b2r:progress:_display
  b2r:print:nl
}

function b2r:progress:set() {
  B2R_PROGRESS_CURRENT="${1}"
  b2r:progress:_display
}