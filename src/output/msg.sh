#!/usr/bin/env bash
# @file Messages
# @brief Set of functions allowing to print text.
# Dependencies
# @require output print special_chars

# @description Debug message.
# Print info on `stderr`, only if debug mode is enabled.
#
# @arg $1 string Message
# @arg $@ string Replacements
# @exitcode 0
# @stderr Formatted message
# @see printf for format options.
function b2r:msg:debug() {
  declare -r MSG="${1}"
  shift
  b2r:print:debug "${MSG}\n" "${@}"
}

# @description Verbose message.
# Print a message only if verbose mode is enabled.
#
# @arg $1 string Message
# @arg $@ string Replacements
# @exitcode 0
# @stdout Formatted message
# @see printf for format options.
function b2r:msg:verbose() {
  declare -r MSG="${1}"
  shift
  b2r:print:verbose "${MSG}\n" "${@}"
}

# @description Normal message.
# Print a message except if quiet mode is enabled.
#
# @arg $1 string Message
# @arg $@ string Replacements
# @exitcode 0
# @stdout Formatted message
# @see printf for format options.
function b2r:msg:output() {
  declare -r MSG="${1}"
  shift
  b2r:print:output "${MSG}\n" "${@}"
}

# @description Important message.
# Print a message. It will always be printed, even in quiet mode.
#
# @arg $1 string Message
# @arg $@ string Replacements
# @exitcode 0
# @stdout Formatted message
# @see printf for format options.
function b2r:msg:important() {
  declare -r MSG="${1}"
  shift
  b2r:print:important "${MSG}\n" "${@}"
}

# @description Informative message.
# Print an informational message, always printed.
#
# @arg $1 string Message
# @arg $@ string Replacements
# @exitcode 0
# @stdout Formatted message
# @see printf for format options.
function b2r:msg:info() {
  declare -r MSG="${1}"
  shift
  b2r:print:info "${MSG}\n" "${@}"
}

# @description Success message.
# Print a message of success, always printed.
#
# @arg $1 string Message
# @arg $@ string Replacements
# @exitcode 0
# @stdout Formatted message
# @see printf for format options.
function b2r:msg:success() {
  declare -r MSG="${1}"
  shift
  b2r:print:success "${MSG}\n" "${@}"
}

# @description Warning message.
# Print a warning message on `stderr`, always printed.
#
# @arg $1 string Message
# @arg $@ string Replacements
# @exitcode 0
# @stderr Formatted message
# @see printf for format options.
function b2r:msg:warning() {
  declare -r MSG="${1}"
  shift
  b2r:print:warning "${MSG}\n" "${@}"
}

# @description Error message.
# Print an error message on `stderr`, always printed.
#
# @arg $1 string Message
# @arg $@ string Replacements
# @exitcode 0
# @stderr Formatted message
# @see printf for format options.
function b2r:msg:error() {
  declare -r MSG="${1}"
  shift
  b2r:print:error "${MSG}\n" "${@}"
}

# @description Panic message and quit script.
# Print an error message on `stderr`, always printed and stop the execution.
#
# @arg $1 string Message
# @arg $@ string Replacements
# @exitcode 255
# @stderr Formatted message
# @see printf for format options.
function b2r:msg:panic() {
  declare -r MSG="${1}"
  shift
  b2r:print:panic "${MSG}\n" "${@}"
}
