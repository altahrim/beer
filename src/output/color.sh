#!/usr/bin/env bash
# @file Color
# @brief Helpers for displaying colors
# Dependencies
# @require output ansi

function b2r:color() {
  b2r:ansi:seq 38 5 "${1}"
}

function b2r:color:back() {
  b2r:ansi:seq 48 5 "${1}"
}

function b2r:color:rgb() {
  b2r:ansi:seq 38 2 "${1:-0}" "${2:-0}" "${3:-0}"
}

function b2r:color:rgb:back() {
  b2r:ansi:seq 48 2 "${1:-0}" "${2:-0}" "${3:-0}"
}
