#!/usr/bin/env bash
# @file Tables
# @brief Functions for drawing tables
# Dependencies
# @require variable var
# @require output ansi

# Variables
declare -gi B2R_TABLE_NB_LINES
declare -gi B2R_TABLE_NB_COLS

# Options
declare -gi B2R_TABLE_MARGIN

# Alignment
declare -gri B2R_TABLE_ALIGN_LEFT=1
declare -gri B2R_TABLE_ALIGN_RIGHT=2
declare -gri B2R_TABLE_ALIGN_CENTER=3

declare -ga B2R_TABLE_COLUMN_ALIGN=()
declare -gri B2R_TABLE_ALIGN_DEFAULT="${B2R_TABLE_ALIGN_RIGHT}"

# Style
declare -ga B2R_TABLE_STYLE_LINE=()
#declare -ga B2R_TABLE_STYLE_LINE_BEFORE=()
#declare -ga B2R_TABLE_STYLE_LINE_AFTER=()
declare -ga B2R_TABLE_STYLE_COL=()
declare -ga B2R_TABLE_STYLE_COL_BEFORE=()
declare -ga B2R_TABLE_STYLE_COL_AFTER=()

# Style positions
declare -xga B2R_TABLE_POS_CONTENT=1
declare -xga B2R_TABLE_POS_BEFORE=2
declare -xga B2R_TABLE_POS_AFTER=4
declare -xga B2R_TABLE_POS_BOTH=$((B2R_TABLE_POS_BEFORE | B2R_TABLE_POS_AFTER))
declare -xga B2R_TABLE_POS_ALL=$((B2R_TABLE_POS_CONTENT | B2R_TABLE_POS_BEFORE | B2R_TABLE_POS_AFTER))

declare -xgi B2R_TABLE_STYLE_LIGHT=0
declare -xgi B2R_TABLE_STYLE_HEAVY=1
declare -xgi B2R_TABLE_STYLE_DOUBLE=2

function b2r:table:add_data() {
  declare -a ARRAY
  for LINE in "${@}"; do
    mapfile -t -d' ' ARRAY < <(echo -n "${LINE}")
    b2r:table:add_line "${ARRAY[@]}"
  done
}

function b2r:table:add_line() {
  declare -ga B2R_TABLE_DATA_"${B2R_TABLE_NB_LINES}"
  (( ++ B2R_TABLE_NB_LINES ))
  declare -n LINE="B2R_TABLE_DATA_${B2R_TABLE_NB_LINES}"
  LINE=("" "${@}")
  if [[ "${#}" -gt "${B2R_TABLE_NB_COLS}" ]]; then
    B2R_TABLE_NB_COLS="${#}"
  fi
}

function b2r:table:add_column() {
  declare -i I=0
  (( ++ B2R_TABLE_NB_COLS ))
  while [[ -n "${*}" ]]; do
    if ! b2r:var:isset "B2R_TABLE_DATA_${I}"; then
      declare -ga B2R_TABLE_DATA_${I}
    fi
    declare -n COL="B2R_TABLE_DATA_${I}"
    #shellcheck disable=SC2034
    COL[${B2R_TABLE_NB_COLS}]="${1}"
    (( ++ I ))
    shift
  done
  if [[ "${I}" -gt "${B2R_TABLE_NB_LINES}" ]]; then
    B2R_TABLE_NB_LINES="${I}"
  fi
}

function b2r:table:set_column_style() {
  declare -r STYLE="${1}"
  declare -ri COLUMN="${2:-${B2R_TABLE_NB_COLS}}"
  declare -ri POS="${3:-B2R_TABLE_POS_CONTENT}"

  if (( B2R_TABLE_POS_CONTENT & POS )); then
    B2R_TABLE_STYLE_COL[${COLUMN}]="${STYLE}"
  fi
  if (( B2R_TABLE_POS_BEFORE & POS )); then
    B2R_TABLE_STYLE_COL_BEFORE[${COLUMN}]="${STYLE}"
  fi
  if (( B2R_TABLE_POS_AFTER & POS )); then
    B2R_TABLE_STYLE_COL_AFTER[${COLUMN}]="${STYLE}"
  fi
}

function b2r:table:set_line_style() {
  echo FIXME
}

function b2r:table:set_margin() {
  if [[ "${1}" -lt 0 ]]; then
    b2r:print:error "Margin can’t be negative."
    return 1
  fi
  B2R_TABLE_MARGIN="${1:-1}"
}

function b2r:table:draw() {
  declare -a COLUMN_WIDTHS=()
  b2r:table:_compute_widths

  declare -i I=0 J=0
  b2r:table:_draw_head
  for I in $(seq 1 "${B2R_TABLE_NB_LINES}"); do
    #shellcheck disable=SC2178
    declare -n LINE="B2R_TABLE_DATA_${I}"
    for J in $(seq 1 "${B2R_TABLE_NB_COLS}"); do
      b2r:table:_draw_cell "${I}" "${J}" "${LINE[${J}]:-}"
      ((++J))
    done
    J=0
    echo
  done
  b2r:table:_draw_tail

  b2r:table:reset
}

function b2r:table:_draw_head() {
  b2r:table:draw_column_separator 0 0
  for COLUMN in $(seq 1 "${B2R_TABLE_NB_COLS}"); do
    NB="${COLUMN_WIDTHS[${COLUMN}]}"
    printf '%*s' "$((B2R_TABLE_MARGIN + NB + B2R_TABLE_MARGIN))" '' | sed 's/ /─/g'
    b2r:table:draw_column_separator "${COLUMN}" 0
  done
  echo
}

function b2r:table:_draw_tail() {
  declare -ri NEXT_LINE="$((B2R_TABLE_NB_LINES + 1))"
  b2r:table:draw_column_separator 0 "${NEXT_LINE}"
  for COLUMN in $(seq 1 "${B2R_TABLE_NB_COLS}"); do
    NB="${COLUMN_WIDTHS[${COLUMN}]}"
    printf '%*s' "$((B2R_TABLE_MARGIN + NB + B2R_TABLE_MARGIN))" '' | sed 's/ /─/g'
    b2r:table:draw_column_separator "${COLUMN}" "${NEXT_LINE}"
  done
  echo
}

function b2r:table:draw_line_separator() {
  # FIXME
  true
}

function b2r:table:draw_column_separator() {
  declare -i COLUMN="${1}"
  declare -i LINE_NO="${2}"
  declare -i COLUMN_NEXT="$((COLUMN + 1))"
  declare HAS_STYLE=false

  if [[ -n "${B2R_TABLE_STYLE_COL_AFTER[${COLUMN}]:-}" ]] || [[ -n "${B2R_TABLE_STYLE_COL_BEFORE[${COLUMN_NEXT}]:-}" ]]; then
    printf '%s' "${B2R_TABLE_STYLE_COL_AFTER[${COLUMN}]:-}${B2R_TABLE_STYLE_COL_BEFORE[${COLUMN_NEXT}]:-}"
    HAS_STYLE=true
  fi

  if [[ "${COLUMN}" -eq 0 ]]; then
    case "${LINE_NO}" in
      0) printf '┌'; ;;
      "$((B2R_TABLE_NB_LINES + 1))") printf '└'; ;;
      *) printf '│'; ;;
    esac
  elif [[ "${COLUMN}" -eq "$((B2R_TABLE_NB_COLS))" ]]; then
    case "${LINE_NO}" in
      0) printf '┐'; ;;
      "$((B2R_TABLE_NB_LINES + 1))") printf '┘'; ;;
      *) printf '│'; ;;
    esac
  else
    case "${LINE_NO}" in
      0) printf '┬'; ;;
      "$((B2R_TABLE_NB_LINES + 1))") printf '┴'; ;;
      *) printf '│'; ;;
    esac
  fi

  if ${HAS_STYLE}; then
    b2r:ansi:reset
  fi
}

function b2r:table:_draw_cell() {
  declare -ri LINE_NO="${1}"
  declare -ri COLUMN="${2}"
  declare -r CONTENT="${3}"
  declare HAS_STYLE

  # Is it first column of line?
  if [[ "${COLUMN}" -eq 1 ]]; then
    b2r:table:draw_column_separator 0 "${LINE_NO}"
  fi

  # Special case, CANCEL character
  if [[ "$(printf "\x18")" = "${CONTENT}" ]]; then
    printf "%*s%*s%*s│" "${B2R_TABLE_MARGIN}" '' "${COLUMN_WIDTHS[${COLUMN}]}" '' "${B2R_TABLE_MARGIN}" '' | sed 's/ /╳/g'
    return
  fi

  # Display content
  declare -i PADDING PADDING_LEFT=0 PADDING_RIGHT=0
  PADDING=$(( "${COLUMN_WIDTHS[${COLUMN}]}" - "${#CONTENT}" ))
  case "${B2R_TABLE_COLUMN_ALIGN[${COLUMN}]:-${B2R_TABLE_ALIGN_DEFAULT}}" in
    "${B2R_TABLE_ALIGN_LEFT}")
      PADDING_RIGHT="${PADDING}"
      ;;
    "${B2R_TABLE_ALIGN_RIGHT}")
      PADDING_LEFT="${PADDING}"
      ;;
    "${B2R_TABLE_ALIGN_CENTER}")
      PADDING_LEFT=$(( PADDING / 2 ))
      PADDING_RIGHT=$(( PADDING - PADDING_LEFT ))
      ;;
  esac

  printf '%*s' "$((B2R_TABLE_MARGIN + PADDING_LEFT))" ''
  HAS_STYLE=false
  if [[ -n "${B2R_TABLE_STYLE_COL[${COLUMN}]:-}" ]] || [[ -n "${B2R_TABLE_STYLE_LINE[${LINE_NO}]:-}" ]]; then
    printf '%s%s' "${B2R_TABLE_STYLE_COL[${COLUMN}]:-}" "${B2R_TABLE_STYLE_LINE[${LINE_NO}]:-}"
    HAS_STYLE=true
  fi
  printf '%s' "${CONTENT}"
  if ${HAS_STYLE}; then
    b2r:ansi:reset
  fi

  printf '%*s' "$((B2R_TABLE_MARGIN + PADDING_RIGHT))" ''

  b2r:table:draw_column_separator "${COLUMN}" "${LINE_NO}"
}

function b2r:table:_compute_widths() {
  declare -i I J
  for I in $(seq 1 "${B2R_TABLE_NB_LINES}"); do
    declare -n COLUMNS="B2R_TABLE_DATA_${I}"
    for J in $(seq 1 "${B2R_TABLE_NB_COLS}"); do
      CELL="${COLUMNS[${J}]:-}"
      if [[ "${#CELL}" -gt "${COLUMN_WIDTHS[${J}]:=0}" ]]; then
          COLUMN_WIDTHS[${J}]="${#CELL}"
      fi
      (( ++ J ))
    done
  done
}

function b2r:table:reset() {
  B2R_TABLE_MARGIN=1
  B2R_TABLE_NB_LINES=0
  B2R_TABLE_NB_COLS=0
  B2R_TABLE_STYLE_LINE=()
  #B2R_TABLE_STYLE_LINE_BEFORE=()
  #B2R_TABLE_STYLE_LINE_AFTER=()
  B2R_TABLE_STYLE_COL=()
  B2R_TABLE_STYLE_COL_BEFORE=()
  B2R_TABLE_STYLE_COL_AFTER=()
}

b2r:table:reset
