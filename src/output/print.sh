#!/usr/bin/env bash
# @file Print
# @brief Set of functions allowing to print text.
# Dependencies
# @require output ansi color

# @description Debug message.
# Print info on `stderr`, only if debug mode is enabled.
#
# @arg $1 string Message
# @arg $@ string Replacements
# @exitcode 0
# @stderr Formatted message
# @see printf for format options.
function b2r:print:debug() {
  if ! ${B2R_DEBUG}; then
    return
  fi
  b2r:color 248 >&2
  # shellcheck disable=SC2059
  printf "${@}" >&2
  b2r:ansi:reset >&2
}

# @description Internal debug message.
# Print info on `stderr`, only if internal debug mode is enabled.
# Reserved for BEER internal usage.
#
# @arg $1 string Message
# @arg $@ string Replacements
# @exitcode 0
# @stderr Formatted message
# @see printf for format options.
function b2r:print:internal_debug() {
  if ! ${B2R_INTERNAL_DEBUG}; then
    return
  fi
  declare -r MSG="${1}"
  shift
  b2r:color 102
  # shellcheck disable=SC2059
  printf "BEER: ${MSG}" "${@}"
  b2r:ansi:reset
}
# @description Verbose message.
# Print a message only if verbose mode is enabled.
#
# @arg $1 string Message
# @arg $@ string Replacements
# @exitcode 0
# @stdout Formatted message
# @see printf for format options.
function b2r:print:verbose() {
  if ! ${B2R_VERBOSE}; then
    return
  fi
  # shellcheck disable=SC2059
  printf "${@}"
}

# @description Normal message.
# Print a message except if quiet mode is enabled.
#
# @arg $1 string Message
# @arg $@ string Replacements
# @exitcode 0
# @stdout Formatted message
# @see printf for format options.
function b2r:print:output() {
  if ${B2R_QUIET}; then
    return
  fi
  # shellcheck disable=SC2059
  printf "${@}"
}

# @description Important message.
# Print a message. It will always be printed, even in quiet mode.
#
# @arg $1 string Message
# @arg $@ string Replacements
# @exitcode 0
# @stdout Formatted message
# @see printf for format options.
function b2r:print:important() {
  # shellcheck disable=SC2059
  printf "${@}"
}

# @description Informative message.
# Print an informational message, always printed.
#
# @arg $1 string Message
# @arg $@ string Replacements
# @exitcode 0
# @stdout Formatted message
# @see printf for format options.
function b2r:print:info() {
  b2r:print:_prefix 'ℹ' 32
  # shellcheck disable=SC2059
  printf "${@}"
}

# @description Success message.
# Print a message of success, always printed.
#
# @arg $1 string Message
# @arg $@ string Replacements
# @exitcode 0
# @stdout Formatted message
# @see printf for format options.
function b2r:print:success() {
  b2r:print:_prefix '🗸' 33
  # shellcheck disable=SC2059
  printf "${@}"
}

# @description Warning message.
# Print a warning message on `stderr`, always printed.
#
# @arg $1 string Message
# @arg $@ string Replacements
# @exitcode 0
# @stderr Formatted message
# @see printf for format options.
function b2r:print:warning() {
  b2r:print:_prefix '⚠' 202 >&2
  # shellcheck disable=SC2059
  printf "${@}" >&2
}

# @description Error message.
# Print an error message on `stderr`, always printed.
#
# @arg $1 string Message
# @arg $@ string Replacements
# @exitcode 0
# @stderr Formatted message
# @see printf for format options.
function b2r:print:error() {
  b2r:print:_prefix '⚠' 124 >&2
  # shellcheck disable=SC2059
  printf "${@}" >&2
}

# @description Panic message and quit script.
# Print an error message on `stderr`, always printed and stop the execution.
#
# @arg $1 string Message
# @arg $@ string Replacements
# @exitcode 255
# @stderr Formatted message
# @see printf for format options.
function b2r:print:panic() {
  b2r:print:_prefix "$(b2r:ansi:bold)⚠" "196" >&2
  # shellcheck disable=SC2059
  printf "${@}" >&2
  exit 255
}

# @description Print newlines
#
# @arg $1 integer Optional number of newlines (default: 1)
# @exitcode 0
# @stdout Linebreak(s)
function b2r:print:nl() {
  declare -ri NB_LINES=${1:-1}
  declare -i I
  # shellcheck disable=SC2034
  for I in $(seq 1 "${NB_LINES}"); do
    echo
  done
}


# @description Print prefix
# @internal
#
# @arg $1 string Symbol to use in prefix
# @arg $2 string Optional color for symbol (default: none).
# @exitcode 0
# @stdout Linebreak(s)
function b2r:print:_prefix() {
  declare -r SYMBOL="${1}"
  declare -r COLOR="${2:-}"
  if [[ -z "${COLOR}" ]]; then
    printf "[ %s ] " "${SYMBOL}"
  else
    printf "[ %s%s%s ] " "$(b2r:color "${COLOR}")" "${SYMBOL}" "$(b2r:ansi:reset)"
  fi
}
