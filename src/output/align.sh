#!/usr/bin/env bash
# @file Text alignment
# @brief Handle alignment of text
# Dependencies
# @require terminal cursor term
# @require output ansi


function b2r:align:right() {
  declare OUTPUT
  declare -i LENGTH
  OUTPUT="$("${@}")"
  LENGTH="$(b2r:ansi:strip "${OUTPUT}" | wc -m)"
  ((-- LENGTH))
  b2r:cursor:move "" "-${LENGTH}"
  echo "${OUTPUT}"
}