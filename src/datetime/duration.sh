#!/usr/bin/env bash
# @file Duration
# @brief Helps to compute and format durations
# Dependencies
# @require output print

# @description Format a duration from float to human readable.
# Largest unit is year and smallest unit is nanosecond.
#
# @arg1 float Number of seconds elapsed.
# @stdout Human readable duration.
function b2r:duration:float_to_human_long() {
  declare -ra UNITS_SINGLE=(year month day hour minute second millisecond microsecond nanosecond)
  declare -ra UNITS_PLURAL=(years months days hours minutes seconds milliseconds microseconds nanoseconds)
  b2r:duration:_float_to_human "${@}"
}

# @description Format a duration from float to human readable, using abbreviations.
# Largest unit is year and smallest unit is nanosecond.
#
# @arg1 float Number of seconds elapsed.
# @stdout Human readable duration.
function b2r:duration:float_to_human() {
  declare -ra UNITS_SINGLE=(y. m. d. h. min. s. ms. µs. ns.)
  declare -ra UNITS_PLURAL=("${UNITS_SINGLE[@]}")
  b2r:duration:_float_to_human "${@}"
}

# @internal
function b2r:duration:_float_to_human() {
  if [[ -z "${1:-}" ]]; then
    b2r:print:error "You must provides a duration as first argument, in seconds."
    return 10
  fi

  declare -a FORMATTED=()
  declare -ri NB_PARTS="${2:-0}"

  declare -a PARSED_TIME
  mapfile -d' ' -t PARSED_TIME < <(date --date="@0${1}" "+%-Y %-m %-d %-H %-M %-S %-N" --utc)
  PARSED_TIME[0]=$((PARSED_TIME[0] - 1970))
  PARSED_TIME[1]=$((PARSED_TIME[1] - 1))
  PARSED_TIME[2]=$((PARSED_TIME[2] - 1))
  
  declare -i UNIT_VALUE
  for I in {0..5}; do
    NB_UNITS="${PARSED_TIME[${I}]}"
    if [[ "${NB_UNITS}" -gt 0 ]]; then
      if [[ "${NB_UNITS}" -gt 1 ]]; then
        UNIT="${UNITS_PLURAL[${I}]}"
      else
        UNIT="${UNITS_SINGLE[${I}]}"
      fi
      FORMATTED=("${FORMATTED[@]}" "${NB_UNITS} ${UNIT}")
    fi
  done

  declare -i NANO_SECONDS="${PARSED_TIME[6]}"
  UNIT_VALUE=1000000000
  for I in {6..8}; do
    UNIT_VALUE=$((UNIT_VALUE / 1000))
    if [[ "${NANO_SECONDS}" -ge "${UNIT_VALUE}" ]]; then
      NB_UNITS=$((NANO_SECONDS / UNIT_VALUE))
      if [[ "${NB_UNITS}" -gt 1 ]]; then
        UNIT="${UNITS_PLURAL[${I}]}"
      else
        UNIT="${UNITS_SINGLE[${I}]}"
      fi
      FORMATTED=("${FORMATTED[@]}" "${NB_UNITS} ${UNIT}")
      NANO_SECONDS="$((NANO_SECONDS - NB_UNITS * UNIT_VALUE))"
    fi
  done

  if [[ "${NB_PARTS}" -gt 0 ]]; then
    FORMATTED=("${FORMATTED[@]:0:${NB_PARTS}}")
  fi
  echo "${FORMATTED[*]}"
}