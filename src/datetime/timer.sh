#!/usr/bin/env bash
# @file Timers
# @brief Set of functions allowing to time commands.
# Dependencies
# @require output print
# @require variable array
# Module variables
declare -gA B2R_TIMERS=()
declare -gr B2R_TIMER_DEFAULT="_default_"

# @description Start a timer.
# A name can be specified with $1, allowing user to start several timers.
#
# @arg $1 string Optional name
# @exitcode 0
function b2r:timer:start() {
  declare -r NAME="${1:-${B2R_TIMER_DEFAULT}}"
  if b2r:array:key_exists "${NAME}" B2R_TIMERS; then
    b2r:print:warning "Timer %s already exists.\\n" "${NAME}"
    echo "VALUE IS ${B2R_TIMERS[${NAME}]}"
  fi
  B2R_TIMERS[${NAME}]="$(date '+%s.%N')"
}

# @description Stop a timer.
# A name can be specified with $1, allowing user to start several timers.
#
# @arg $1 string Optional name
#
# @stdout Time elapsed since timer started as <seconds>.<nanoseconds>
# @exitcode 0 if sucessful
# @exitcode 1 if no timer started with this name
function b2r:timer:stop() {
  declare -r NAME="${1:-${B2R_TIMER_DEFAULT}}"
  if ! b2r:array:key_exists "${NAME}" B2R_TIMERS; then
    b2r:print:error "Timer %s not found." "${NAME}"
  fi

  <<< "$(date '+%s.%N') - ${B2R_TIMERS[${NAME}]}" bc -l
}
