#!/usr/bin/env bash
# @file Variable helpers
# @brief Collection of helpers for bash variables

# @description Checks if a key exists in selected array.
#
# @arg $1 string Variable name
# @exitcode 0 if variable is defined (even empty)
# @exitcode 1 if variable isn't defined
function b2r:var:isset() {
  # TODO Test
  declare | grep -q "^${1}="
}
