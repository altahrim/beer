#!/usr/bin/env bash
# @file String helpers
# @brief Collection of helpers for bash strings

function b2r:string:repeat() {
  declare -ri NB="${1:- }"
  declare -r STRING="${2:- }"
  declare -i I
  #shellcheck disable=SC2034
  for I in $(seq 1 "${NB}"); do
    echo -n "${STRING}"
  done
}
