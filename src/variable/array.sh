#!/usr/bin/env bash
# @file Array helpers
# @brief Collection of helpers for bash array
# Dependencies
# @require variable var

# @description Checks if a key exists in selected array.
#
# @arg $1 string Searched key
# @arg $2 string Array name
# @exitcode 0 if key exists
# @exitcode 1 if key doesn't exist
# @exitcode 2 if array is empty
# @exitcode 3 if array doesn't exist
function b2r:array:key_exists() {
  declare -r NEEDLE="${1}"
  if ! b2r:var:isset "${2}"; then
    return 3
  fi
  declare -rn B2R_INTERNAL_ARRAY="${2}"
  if [[ -z "${B2R_INTERNAL_ARRAY[*]:-}" ]]; then
    return 2
  fi
  for KEY in "${!B2R_INTERNAL_ARRAY[@]}"; do
    if [[ "${KEY}" = "${NEEDLE}" ]]; then
      return 0
    fi
  done
  return 1
}

# @description Checks if a value exists in selected array.
#
# @arg $1 string Searched value
# @arg $2 string Array name
# @exitcode 0 if key exists
# @exitcode 1 if key doesn't exist
# @exitcode 2 if array is empty
# @exitcode 3 if array doesn't exist
function b2r:array:exists() {
  declare -r NEEDLE="${1}"
  if ! b2r:var:isset "${2}"; then
    return 3
  fi
  declare -rn B2R_INTERNAL_ARRAY="${2}"
  if [[ -z "${B2R_INTERNAL_ARRAY[*]:-}" ]]; then
    return 2
  fi
  declare VALUE

  for VALUE in "${B2R_INTERNAL_ARRAY[@]}"; do
    if [[ "${VALUE}" = "${NEEDLE}" ]]; then
      return 0
    fi
  done

  return 1
}

# @description Returns the index of the first occurrence of searched value.
#
# @arg $1 string Searched value
# @arg $2 string Array name
# @stdout Index of selected value if successful.
# @exitcode 0 if value exists.
# @exitcode 1 if value doesn’t exist.
function b2r:array:find() {
  declare -r NEEDLE="${1}"
  declare -rn B2R_INTERNAL_ARRAY="${2}"
  declare INDEX VALUE

  for INDEX in "${!B2R_INTERNAL_ARRAY[@]}"; do
    VALUE="${B2R_INTERNAL_ARRAY[${INDEX}]}"
    if [[ "${VALUE}" == "${NEEDLE}" ]]; then
      echo "${INDEX}"
      return 0
    fi
  done

  return 1
}

# @description Joins an array with defined separator.
#
# @arg $1 string Separator (only first character is used)
# @arg $2 string Array name
# @exitcode 0 if successful
# @stdout Imploded array
function b2r:array:implode() {
  declare -r IFS="${1}"
  declare -rn B2R_INTERNAL_ARRAY="${2}"
  echo "${B2R_INTERNAL_ARRAY[*]}"
}

# @description Push one or more elements at the end of target array.
#
# @arg $1 string Array name
# @arg $... string Elements to push
# @exitcode 0 if successful.
# @exitcode 1 if target array doesn't exist.
function b2r:array:push() {
  if ! b2r:var:isset "${1}"; then
    return 1
  fi
  declare -n B2R_INTERNAL_ARRAY="${1}"
  shift
  if [[ -z "${B2R_INTERNAL_ARRAY[*]:-}" ]]; then
    B2R_INTERNAL_ARRAY=("${@}")
  else
    B2R_INTERNAL_ARRAY=("${B2R_INTERNAL_ARRAY[@]}" "${@}")
  fi
}
