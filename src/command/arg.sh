#!/usr/bin/env bash
# @file Parse command options
# @brief Getters and setters for options
# Dependencies
# @require variable array
# @require command help
# @require output msg

declare -gri B2R_ARG_OPT_NO_VALUE=0
declare -gri B2R_ARG_OPT_OPTIONAL=1
declare -gri B2R_ARG_OPT_REQUIRED=2

declare -g B2R_OPTIONS_CMD_NAME=""
# FIXME Integrate them
#declare -g B2R_OPTIONS_CMD_DESC=""
#declare -g B2R_OPTIONS_CMD_VERSION=""
#declare -g B2R_OPTIONS_CMD_LICENCE=""
#declare -ga B2R_OPTIONS_CMD_AUTHORS=()

declare -ga B2R_OPTIONS_SHORT=()
declare -ga B2R_OPTIONS_LONG=()
declare -ga B2R_OPTIONS_REQUIRED=()
declare -ga B2R_OPTIONS_TARGET=()
#shellcheck disable=SC2034
declare -ga B2R_OPTIONS_HELP=()

#shellcheck disable=SC2034
declare -ga B2R_ARGUMENTS_NAME=()
#shellcheck disable=SC2034
declare -ga B2R_ARGUMENTS_HELP=()
#shellcheck disable=SC2034
declare -ga B2R_ARGUMENTS_MULTIPLE=()
#shellcheck disable=SC2034
declare -ga B2R_ARGUMENTS_MANDATORY=()

declare -xgA B2R_OPTIONS=()
declare -xga B2R_ARGUMENTS=()


function b2r:arg:use_common_opts() {
  b2r:arg:add_option h help b2r:help:display_and_quit 'displays this help and exit.'
  b2r:arg:add_option v verbose B2R_VERBOSE 'displays more text.'
  b2r:arg:add_option d debug B2R_DEBUG 'displays debug information on stderr.'
  b2r:arg:add_option q quiet B2R_QUIET 'displays only important messages.'
  b2r:arg:add_option - '' '' 'all options met after this option will be ignored.'
}

function b2r:arg:parse() {
  declare PARSED_OPTIONS SHORT_OPTS LONG_OPTS
  SHORT_OPTS="$(b2r:arg:_build_short_opts)"
  LONG_OPTS="$(b2r:arg:_build_long_opts)"
  if [[ -z "${B2R_OPTIONS_CMD_NAME}" ]]; then
    B2R_OPTIONS_CMD_NAME="${0}"
  fi
  if ! PARSED_OPTIONS="$(getopt -n "${B2R_OPTIONS_CMD_NAME}" -o "${SHORT_OPTS}" --long "${LONG_OPTS}" -- "${@}")"; then
    b2r:print:error "At least one error found during option parsing. Please se above.\n"
    return 1
  fi
  eval set -- "${PARSED_OPTIONS}"
  while [[ ${#} -gt 0 ]]; do
    if [[ ${1} == '--' ]]; then
      shift
      break;
    fi

    declare SHIFT=1
    if ! b2r:arg:_try_short_option "${1}" "${2}"; then
      if ! b2r:arg:_try_long_option "${1}" "${2}"; then
        b2r:print:error "Unrecognized option %s.\n" "${1}"
      fi
    fi

    shift "${SHIFT}"
  done
  B2R_ARGUMENTS=("${@}")
}

function b2r:arg:_try_short_option() {
  declare -i INDEX
  if ! INDEX="$(b2r:array:find "${1:1}" B2R_OPTIONS_SHORT)"; then
    return 1
  fi

  b2r:arg:_set_option "${INDEX}" "${2}"
}

function b2r:arg:_try_long_option() {
  declare -i INDEX
  if ! INDEX="$(b2r:array:find "${1:2}" B2R_OPTIONS_LONG)"; then
    return 1
  fi

  b2r:arg:_set_option "${INDEX}" "${2}"
}

function b2r:arg:_set_option() {
  declare -r INDEX="${1}"
  declare VALUE="${2}"
  declare -r SHORT="${B2R_OPTIONS_SHORT[${INDEX}]}"
  declare -r LONG="${B2R_OPTIONS_LONG[${INDEX}]}"
  declare -ri REQUIRED="${B2R_OPTIONS_REQUIRED[${INDEX}]}"
  declare -r VARIABLE_NAME="${B2R_OPTIONS_TARGET[${INDEX}]}"
  if [[ "${REQUIRED}" -gt "${B2R_ARG_OPT_NO_VALUE}" ]]; then
    SHIFT=2
  else
    VALUE=true
  fi

  if [[ -n "${SHORT}" ]]; then
    B2R_OPTIONS["${SHORT}"]="${VALUE}"
  fi

  if [[ -n "${LONG}" ]]; then
    B2R_OPTIONS["${LONG}"]="${VALUE}"
  fi

  # If callback or variable is provided, use it
  if [[ -n "${VARIABLE_NAME}" ]]; then
    if declare -Fp "${VARIABLE_NAME}" >& /dev/null; then
      # Target is a function, we must execute it.
      # Potential value is provided as first argument.
      "${VARIABLE_NAME}" "${2}"
    else
      # Otherwise, target is a variable, we have to fill it.
      declare -n VARIABLE="${VARIABLE_NAME}"
      #shellcheck disable=SC2034
      VARIABLE="${VALUE}"
    fi
  fi

  return 0
}

function b2r:arg:_build_short_opts() {
  for OPT_INDEX in "${!B2R_OPTIONS_SHORT[@]}"; do
    OPT="${B2R_OPTIONS_SHORT[$OPT_INDEX]}"
    if [[ -z "${OPT}" ]]; then
      continue
    fi
    echo -n "${OPT}"
    case "${B2R_OPTIONS_REQUIRED[$OPT_INDEX]}" in
      "${B2R_ARG_OPT_OPTIONAL}")
        echo -n '::'
        ;;
      "${B2R_ARG_OPT_REQUIRED}")
        echo -n ':'
        ;;
    esac
  done
}

function b2r:arg:_build_long_opts() {
  #shellcheck disable=SC2034
  declare -a OPTS=()
  for OPT_INDEX in "${!B2R_OPTIONS_LONG[@]}"; do
    OPT="${B2R_OPTIONS_LONG[$OPT_INDEX]}"
    if [[ -z "${OPT}" ]]; then
      continue
    fi
    case "${B2R_OPTIONS_REQUIRED[$OPT_INDEX]}" in
      "${B2R_ARG_OPT_OPTIONAL}")
        OPT="${OPT}::"
        ;;
      "${B2R_ARG_OPT_REQUIRED}")
        OPT="${OPT}:"
        ;;
    esac
    b2r:array:push OPTS "${OPT}"
  done
  b2r:array:implode ',' OPTS
}

# @description Add a new option to allowed options.
# This option will be parsed by b2r:arg:parse and displayed in help if an help message is provided.
#
# @arg $1 string Short option name (only one character allowed)
# @arg $2 string Long option name
# @arg $3 string Target if option is provided by user.
#  If a function is provided, it will be called with option name et option value.
#  If a variable is provided, it will be populated with option value.
# @arg $4 string Help text.
# @arg $5 int Indicate if option need a value.
#  Possible values are B2R_ARG_OPT_NO_VALUE, B2R_ARG_OPT_OPTIONAL and B2R_ARG_OPT_REQUIRED.
# @arg $6 string Optional type. TODO Implement type.
# @exitcode 0 if successful
# @exitcode 1 if no short and no long alias provided.
# @exitcode 2 if short option contains more than one character.
# @exitcode 3 if short option is already used for another option.
# @exitcode 4 if long option contains only one character.
# @exitcode 5 if long option is already used for another option.
function b2r:arg:add_option() {
  declare -r SHORT_OPT="${1:-}"
  declare -r LONG_OPT="${2:-}"
  declare -r TARGET="${3:-}"
  declare -r HELP="${4:-}"
  declare -r REQUIRED="${5:-${B2R_ARG_OPT_NO_VALUE}}"
  #declare -r TYPE="${6:-}"

  if [[ -z "${SHORT_OPT}" ]] && [[ -z "${LONG_OPT}" ]]; then
    b2r:print:error "You have to add at least a short or a long alias."
    return 1
  fi

  if [[ -n "${SHORT_OPT}" ]]; then
    if [[ "${#SHORT_OPT}" -gt 1 ]]; then
      b2r:print:error "Only one character allowed for short option."
      b2r:print:output "\"${SHORT_OPT}\" given.\n"
      return 2
    fi
    if b2r:array:exists "${SHORT_OPT}" B2R_OPTIONS_SHORT; then
      b2r:print:error "Short option %s already exists." "${SHORT_OPT}"
      b2r:print:nl
      return 3
    fi
  fi

  if [[ -n "${LONG_OPT}" ]]; then
    if [[ "${#LONG_OPT}" -le 1 ]]; then
      b2r:print:error "For long option, use at least two characters."
      b2r:print:output "\"${LONG_OPT}\" given.\n"
      return 4
    fi
    if b2r:array:exists "${LONG_OPT}" B2R_OPTIONS_LONG; then
      b2r:msg:error "Long option %s already exists." "${LONG_OPT}"
      return 5
    fi
  fi

  case "${REQUIRED}" in
    "${B2R_ARG_OPT_NO_VALUE}"|"${B2R_ARG_OPT_OPTIONAL}"|"${B2R_ARG_OPT_REQUIRED}") ;; # Valid values
    *)
      b2r:print:error "Option type must be one of B2R_ARG_OPT_NO_VALUE, B2R_ARG_OPT_OPTIONAL or B2R_ARG_OPT_REQUIRED\n"
      b2r:print:output "\"${REQUIRED}\" given.\n"
      ;;
  esac

  b2r:array:push B2R_OPTIONS_SHORT "${SHORT_OPT}"
  b2r:array:push B2R_OPTIONS_LONG "${LONG_OPT}"
  b2r:array:push B2R_OPTIONS_REQUIRED "${REQUIRED}"
  b2r:array:push B2R_OPTIONS_TARGET "${TARGET}"
  b2r:array:push B2R_OPTIONS_HELP "${HELP}"
}

# @description Declares a new argument.
# Arguments are only used for displaying help. No parsing or error handling is done using this configuration.
#
# @arg $1 string Argument name
# @arg $2 string Related help
# @arg $3 boolean Mandatory (true/false)
# @arg $4 boolean Multiple values allowed (true/false)
# @exitcode 0 if successful
function b2r:arg:add_argument() {
  declare -r NAME="${1}"
  declare -r HELP="${2:-}"
  declare -r MANDATORY="${3:-true}"
  declare -r MULTIPLE="${4:-false}"

  b2r:array:push B2R_ARGUMENTS_NAME "${NAME}"
  b2r:array:push B2R_ARGUMENTS_HELP "${HELP}"
  b2r:array:push B2R_ARGUMENTS_MULTIPLE "${MULTIPLE}"
  b2r:array:push B2R_ARGUMENTS_MANDATORY "${MANDATORY}"
}
