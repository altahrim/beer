#!/usr/bin/env bash
# @file Help
# @brief Rely on option.sh to display help
# Dependencies
# @require command arg
# @require output print ansi

function b2r:help:display() {
  b2r:print:output "%s (version %s)" "$(b2r:ansi:bold)$(basename "${B2R_OPTIONS_CMD_NAME}")$(b2r:ansi:reset)" "${B2R_OPTIONS_CMD_VERSION:-development}"
  b2r:print:nl

  b2r:help:_display_header "Usage"
  b2r:print:important "%s [options] [arguments]" "${B2R_OPTIONS_CMD_NAME}"
  b2r:print:nl

  b2r:help:_display_header "Options"
  b2r:help:_get_options | sort -h | column -s'|' -t -o ''

  b2r:help:_display_header "Arguments"
  b2r:help:_get_arguments | sort -h | column -s'|' -t -o ''
}

function b2r:help:_display_header() {
  b2r:print:nl
  b2r:ansi:bold
  b2r:ansi:underline
  b2r:color 32
  b2r:print:important "▶ "
  b2r:print:important "${@}"
  b2r:print:important ":\n"
  b2r:ansi:reset
}

function b2r:help:_get_options() {
  declare -i I
  declare SHORT_OPT LONG_OPT HELP_OPT
  for I in "${!B2R_OPTIONS_HELP[@]}"; do
    if [[ -z "${B2R_OPTIONS_HELP[${I}]}" ]]; then
      continue
    else
      HELP_OPT="${B2R_OPTIONS_HELP[${I}]}"
    fi

    if [[ -n "${B2R_OPTIONS_SHORT[${I}]}" ]]; then
      SHORT_OPT="-${B2R_OPTIONS_SHORT[${I}]}"
      case "${B2R_OPTIONS_REQUIRED[${I}]}" in
        "${B2R_ARG_OPT_OPTIONAL}") SHORT_OPT="${SHORT_OPT}[val]";;
        "${B2R_ARG_OPT_REQUIRED}") SHORT_OPT="${SHORT_OPT}<val>";;
      esac
    else
      SHORT_OPT=""
    fi
    if [[ -n "${B2R_OPTIONS_LONG[${I}]}" ]]; then
      LONG_OPT="--${B2R_OPTIONS_LONG[${I}]}"
      case "${B2R_OPTIONS_REQUIRED[${I}]}" in
        "${B2R_ARG_OPT_OPTIONAL}") LONG_OPT="${LONG_OPT}[=val]";;
        "${B2R_ARG_OPT_REQUIRED}") LONG_OPT="${LONG_OPT}=<val>";;
      esac
    else
      LONG_OPT=""
    fi

    if [[ -z "${SHORT_OPT}" ]] || [[ -z "${LONG_OPT}" ]]; then
      SEP=' '
    else
      SEP=','
    fi
    echo "  ${SHORT_OPT}|${SEP}| ${LONG_OPT}|: ${HELP_OPT}"
  done
}

function b2r:help:_get_arguments() {
  declare NAME HELP
  for I in "${!B2R_ARGUMENTS_HELP[@]}"; do
    NAME="${B2R_ARGUMENTS_NAME[${I}]}"
    if ${B2R_ARGUMENTS_MULTIPLE[${I}]}; then
      NAME="${NAME}…"
    fi
    if ! ${B2R_ARGUMENTS_MANDATORY[${I}]}; then
      NAME="[${NAME}]"
    fi
    HELP="${B2R_ARGUMENTS_HELP[${I}]}"
    echo "  ${NAME}|: ${HELP}"
  done
}

function b2r:help:display_and_quit() {
  b2r:help:display
  exit 0
}
