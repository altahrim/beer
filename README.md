## Why BEER.sh?
**BEER** stands for **B**ash **E**asy **E**ntropy **R**emover.
It aims to help you build reliable bash scripts by enforcing bash best practices and providing some high level functions I tend to rewrite in all my projects.

## How to ~~drink~~ use it ?
### Installation
You can clone this repository on your machine :
```bash
git clone https://gitlab.com/altahrim/beer.git /usr/local/share/beer
```

### Using it
In your scripts, you only have to include `beer.sh` and import the modules you need :
```bash
# Source BEER.sh
source /usr/local/share/beer/beer.sh

# Import modules
b2r:import output print
b2r:import command

# Enjoy
b2r:options:parse "${@}"
b2r:print:output "Hello world!"
```
See `examples` directory for more fun!
