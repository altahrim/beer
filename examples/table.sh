#!/usr/bin/env bash
# shellcheck disable=SC1117
# shellcheck source=beer.sh
source "$(dirname "$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")")/beer.sh"
b2r:import output table ansi color

b2r:table:add_data 'XOR 0 1' '0 0 1' '1 1 0'
#b2r:table:display_header true
b2r:table:draw

b2r:table:add_line 'Column 1' 'Column 2' 'Another one'
b2r:table:add_column 1 2 3 4
b2r:table:add_column 5 6 "$(printf '\x18')" 7 8
#b2r:table:add_line - - "$(printf 'Multi\nLine')" - - - -
b2r:table:add_line A BB CCC DDDD EEEEE FFFFFF ☺
b2r:table:set_margin 3
b2r:table:set_column_style "$(b2r:ansi:bold)" 1
b2r:table:set_column_style "$(b2r:color 33)" 1 B2R_TABLE_POS_AFTER
b2r:table:draw
