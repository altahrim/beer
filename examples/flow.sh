#!/usr/bin/env bash
# shellcheck disable=SC1117
# shellcheck source=beer.sh
source "$(dirname "$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")")/beer.sh"
b2r:import output action

b2r:print:output "You can explain what you are doing:\n"
b2r:action:start "Start an action"
# Do some stuff
sleep 0.3
b2r:action:end

b2r:action:start "Start action with happy ending"
sleep 0.3
b2r:action:success

b2r:action:start "Start initial action"
b2r:action:start "Start a sub-action"
b2r:action:start "And why not a third one"
sleep 0.3
b2r:action:success
sleep 0.2
b2r:action:failure
sleep 0.1
b2r:action:success


b2r:action:start "Start bad action"
sleep 0.3
b2r:action:failure

b2r:action:exec "Action success in oneliner" sleep 1
b2r:action:exec "Action failure in oneliner" ls /no/directory/like/this
