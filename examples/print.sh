#!/usr/bin/env bash
# shellcheck disable=SC1117
# shellcheck source=beer.sh
source "$(dirname "$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")")/beer.sh"
b2r:import output print align

b2r:print:important "⯈ Classic message: it will be printed everytime, except in quiet mode.\n"
b2r:print:output "This is a classic message\n"

b2r:print:important "⯈ Important message: it will be printed everytime.\n"
b2r:print:important "You should really read this!\n"

b2r:print:important "⯈ Debug message: it will be printed only in debug mode.\n"
b2r:print:debug "Some kind of debug information.\n"

b2r:print:important "⯈ Verbose message: it will be printed only in verbose mode.\n"
b2r:print:verbose "I can be chatty if asked.\n"

b2r:print:nl 2
b2r:align:right b2r:print:important "\e[1mAny\e[0m function can be written on right of the screen.\n"

b2r:print:nl 2
b2r:print:important "⯈⯈ Error handling (always printed) :\n"
b2r:print:important "⯈ Informative message\n"
b2r:print:info "For now, everything is looking good\n"

b2r:print:important "⯈ Success\n"
b2r:print:success "Oh, still alive 🙂\n"

b2r:print:important "⯈ Warning\n"
b2r:print:warning "It looks like we are reaching the end of the script :|\n"

b2r:print:important "⯈ Error\n"
b2r:print:error "Error 42: this smell very bad.\n"

b2r:print:important "⯈ Panic: script will fail with 255 as error code\n"
b2r:print:panic "Oh no, something very nasty happened… We should stop here.\n"
