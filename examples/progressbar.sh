#!/usr/bin/env bash
# shellcheck disable=SC1117
# shellcheck source=beer.sh
source "$(dirname "$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")")/beer.sh"
b2r:import output print progress

b2r:print:output "Try to load 125 things…"
b2r:progress:init 125
while b2r:progress:next; do
  # Insert your process here
  sleep 0.01
done

b2r:print:nl
b2r:print:output "This one seems a bit buggy…"
b2r:progress:init 50
for I in 0 25 40 30 12 49 50; do
  sleep 0.2
  b2r:progress:set "${I}"
done

b2r:print:nl
b2r:print:output "Sometimes, shit happens…"
b2r:progress:init 100
for I in 0 25 40 30 12 49 50; do
  sleep 0.2
  b2r:progress:set "${I}"
done