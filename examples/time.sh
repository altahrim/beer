#!/usr/bin/env bash
# shellcheck disable=SC1117
# shellcheck source=beer.sh
source "$(dirname "$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")")/beer.sh"
b2r:import datetime

declare -ra DURATIONS=(1 '1.3' 123 664575 '7675.76757' '7576574900.00000098000000001' '.000000001')
#declare -ra DURATIONS=('7576574900.00000098000000001')

b2r:print:important "⯈ Format durations in seconds as short human readable string.\n"
for DURATION in "${DURATIONS[@]}"; do
  b2r:print:important "\e[1m%30s second(s):\e[0m %s\n" "${DURATION}" "$(b2r:duration:float_to_human "${DURATION}")"
done

b2r:print:nl
b2r:print:important "⯈ Format durations in seconds as long human readable string.\n"
for DURATION in "${DURATIONS[@]}"; do
  b2r:print:important "\e[1m%30s second(s):\e[0m %s\n" "${DURATION}" "$(b2r:duration:float_to_human_long "${DURATION}")"
done

b2r:print:nl
b2r:print:important "⯈ Format durations in seconds as abbreviations with only two most significatives parts).\n"
for DURATION in "${DURATIONS[@]}"; do
  b2r:print:important "\e[1m%30s second(s):\e[0m %s\n" "${DURATION}" "$(b2r:duration:float_to_human "${DURATION}" 2)"
done

b2r:print:nl
b2r:print:important "⯈ You can time commands in your script\n"
b2r:timer:start example
sleep 1
b2r:timer:stop example
