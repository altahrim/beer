#!/usr/bin/env bash
# shellcheck disable=SC1117
# shellcheck source=beer.sh
source "$(dirname "$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")")/beer.sh"
b2r:import command arg

b2r:arg:use_common_opts
b2r:arg:add_option '' 'long-option' '' 'This is a useless long option…'
b2r:arg:add_option 's' '' '' 'This is a useless short option…'
b2r:arg:add_option 'i' 'integer' INTEGER 'This option needs a value' "${B2R_ARG_OPT_REQUIRED}"
b2r:arg:add_option 't' 'text' TEXT 'This option can have a value' "${B2R_ARG_OPT_OPTIONAL}"
b2r:arg:add_option 'f' '' opt_callback 'This option will trigger function opt_callback when provided.' "${B2R_ARG_OPT_OPTIONAL}"
b2r:arg:add_option 'x' '' HIDDEN_OPT
b2r:arg:add_argument 'arg1' 'first mandatory argument'
b2r:arg:add_argument 'next_args' 'remaining optional arguments' false true

declare -i INTEGER=-1
declare TEXT="_default_"
function opt_callback() {
  echo '#'
  echo "# Callback called"
  echo "# Command line: ${*}"
  echo '#'
}
declare HIDDEN_OPT=false

b2r:arg:parse "${@}"

b2r:print:output "This script parses options.\nYou can display help with -h and play with different combinations.\n"

b2r:print:debug "Debug: we’ve been here.\n"
b2r:print:important "# Options found:\n"
for OPT_NAME in "${!B2R_OPTIONS[@]}"; do
  b2r:print:important " - Option %30s: %s\n" "${OPT_NAME}" "${B2R_OPTIONS[${OPT_NAME}]}"
done
b2r:print:output "For convenience, each option appears under its short and long alias.\n"

b2r:print:verbose "My friends call me Verbal 'cause they think I talk too much.\n"
b2r:print:important "# Arguments found:\n"
for VALUE in "${B2R_ARGUMENTS[@]}"; do
  b2r:print:important " - %s\n" "${VALUE}"
done

b2r:print:nl
b2r:print:important 'Provided integer is "%d"\n' "${INTEGER}"
b2r:print:important 'Provided text is "%s"\n' "${TEXT}"
b2r:print:important 'Hidden option -x "%s"\n' "${HIDDEN_OPT}"
