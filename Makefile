##################################################
### Variables                                  ###
##################################################
# You can change following variables at runtime
# ie: PREFIX=/usr make install
PROG ?= beer
PREFIX ?= /usr/local
LIBDIR ?= ${PREFIX}/share


##################################################
### Introduction / Help                        ###
##################################################
help:
	echo '$(PROG) is a bash library which does not need compilation. Simply load it in your bash scripts.'
	echo
	echo 'You can install it on your system with "sudo make install".'
	echo 'Then you will be able to use it with:'
	echo ' source /usr/local/share/beer/beer.sh'
	echo
	echo 'Check README.md and examples directory for more information.'
	echo


##################################################
### Tests                                      ###
##################################################
lint: shellcheck check_todo
test: shunit

shellcheck:
	@printf "\\e[48;5;24;1m %-80s\\e[0m\\n" "Launch ShellCheck…"
	$(MAKE) -k $(addsuffix /shellcheck,$(shell find . -type f -name '*.sh'))
	@echo

%/shellcheck:
	@if ! git check-ignore -q '$(@D)'; then \
	  printf '\e[38;5;250mShellCheck: %s \e[0m\n' '$(@D)'; \
	  shellcheck -S style -x -Calways '$(@D)'; \
	fi

shunit:
	@printf "\\e[48;5;24;1m %-80s\\e[0m\\n" "Launch shUnit checks…"
	$(MAKE) -k $(addsuffix /shunit,$(shell find tests/ -type f -executable -name '*.sh'))
	@echo

%/shunit:
	@if ! git check-ignore -q '$(@D)'; then \
	  printf '\e[38;5;250mShUnit: %s \e[0m\n' '$(@D)'; \
	  './$(@D)'; \
	fi

check_todo:
	@printf "\\e[48;5;24;1m %-80s \\e[0m\\n" "Check remaining TODO or FIXME…"
	grep -Rin --color=always '#\s*FIXME' .
	grep -Rin --color=always '#\s*TODO' .
	@echo


##################################################
### Installation                               ###
##################################################
install:
	install -v -m0755 -d '$(LIBDIR)/$(PROG)'
	install -v -m0755 beer.sh '$(LIBDIR)/$(PROG)/$(PROG).sh'
	rsync -rlvp --delete src '$(LIBDIR)/$(PROG)/'

uninstall:
	rm -vrf '$(LIBDIR)/$(PROG)'


##################################################
.SILENT: help install uninstall shellcheck
.PHONY: help lint test shellcheck shunit check_todo install uninstall
