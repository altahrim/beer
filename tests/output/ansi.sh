#!/usr/bin/env bash
# @file Test suite for ansi output
# shellcheck source=tests/init.sh
source "$(dirname "$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")")/init.sh"

function testAnsiStrip() {
  declare ANSI_STRING BASIC_STRING
  ANSI_STRING="$(b2r:ansi:seq 48 5 25)Colored $(b2r:ansi:bold)and bold$(b2r:ansi:reset)"
  BASIC_STRING="Colored and bold"

  assertEquals "ANSI sequences must be completely removed" "${BASIC_STRING}" "$(b2r:ansi:strip "${ANSI_STRING}")"
}