#!/usr/bin/env bash
# @file Test suite for array helpers
# shellcheck source=tests/init.sh
source "$(dirname "$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")")/init.sh"

function testAddOptionValid() {
  assertTrue "b2r:arg:add_option a aaa '' 'Some help'"
}

function testAddOptionWithNoAliasProvided() {
  assertErrorContains "Function must fail when no short or long alias are provided" \
    1 \
    "You have to add at least a short or a long alias." \
    "b2r:arg:add_option"
}

function testAddOptionWithShortOptContainsMoreThanOneCharacter() {
  assertErrorContains "Function must fail when short option contains more than one character" \
    2 \
    "Only one character allowed for short option." \
    "b2r:arg:add_option aa"
}

function testAddOptionWithAlreadyExistingShortOption() {
  b2r:arg:add_option h
  assertErrorContains "Function must fail when a short option is added twice" \
    3 \
    "Short option h already exists." \
    "b2r:arg:add_option h"
}

function testAddOptionWithLongOptionContainingOnlyOneCharacter() {
  assertErrorContains "Function must fail when a long option contains less than two characters" \
    4 \
    "For long option, use at least two characters." \
    "b2r:arg:add_option p m"
}

function testAddOptionWithAlreadyExistingLongOption() {
  b2r:arg:add_option '' dummy
  assertErrorContains "Function must fail when a long option is added twice" \
    5 \
    "Long option dummy already exists." \
    "b2r:arg:add_option z dummy"
}