#!/usr/bin/env bash
# @file Test suite for array helpers
# shellcheck source=tests/init.sh
source "$(dirname "$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")")/init.sh"

function testHumanShortFormatEmpty() {
  assertEquals "If duration is eqal to zero, function must print nothing." "" "$(b2r:duration:float_to_human 0)"
}

function testHumanLongFormatEmpty() {
  assertEquals "If duration is eqal to zero, function must print nothing." "" "$(b2r:duration:float_to_human_long 0)"
}

function testHumanShortFormatInteger() {
  assertEquals "1 s." "$(b2r:duration:float_to_human 1)"
  assertEquals "2 min. 3 s." "$(b2r:duration:float_to_human 123)"
  assertEquals "1 d. 10 h. 17 min. 36 s." "$(b2r:duration:float_to_human 123456)"
}

function testHumanLongFormatInteger() {
  assertEquals "1 second" "$(b2r:duration:float_to_human_long 1)"
  assertEquals "2 minutes 3 seconds" "$(b2r:duration:float_to_human_long 123)"
  assertEquals "1 day 10 hours 17 minutes 36 seconds" "$(b2r:duration:float_to_human_long 123456)"
}

function testHumanShortFormatFloat() {
  assertEquals "1 d. 1 h. 1 min. 1 s. 1 ms. 1 µs. 1 ns." "$(b2r:duration:float_to_human 90061.001001001)"
  assertEquals "7 ms." "$(b2r:duration:float_to_human .007)"
}

function testHumanLongFormatFloat() {
  assertEquals "1 day 1 hour 1 minute 1 second 1 millisecond 1 microsecond 1 nanosecond" "$(b2r:duration:float_to_human_long 90061.001001001)"
  assertEquals "7 milliseconds" "$(b2r:duration:float_to_human_long .007)"
}

function testHumanShortFormatMaxParts() {
  assertEquals "Only the two most meaningful parts must be printed." "1 d. 1 h." "$(b2r:duration:float_to_human 90061.001001 2)"
}

function testHumanLongFormatMaxParts() {
  assertEquals "Only the two most meaningful parts must be printed." "1 day 1 hour" "$(b2r:duration:float_to_human_long 90061.001001 2)"
}

function testHumanShortFormatNoArgument() {
  OUTPUT="$(b2r:duration:float_to_human 2>&1)"
  RETURN_CODE="$?"
  assertEquals "Function must fail if no argument is provided." 10 ${RETURN_CODE}
  assertContains "${OUTPUT}" "You must provides a duration as first argument, in seconds."
}

function testHumanLongFormatNoArgument() {
  OUTPUT="$(b2r:duration:float_to_human_long 2>&1)"
  RETURN_CODE="$?"
  assertEquals "Function must fail if no argument is provided." 10 ${RETURN_CODE}
  assertContains "${OUTPUT}" "You must provides a duration as first argument, in seconds."
}
