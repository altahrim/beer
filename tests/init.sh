#!/usr/bin/env bash
# shellcheck source=beer.sh
source "$(dirname "$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")")/beer.sh"

# @description Import shUnit2 to launch tests
function launch_shunit() {
  declare SHUNIT_INIT_FILE
  SHUNIT_INIT_FILE="$(search_shunit)"
  # Source tested module
  declare B2R_MODULE B2R_FILE
  B2R_MODULE="$(basename "$(dirname "${0}")")"
  B2R_FILE="$(basename "${0}" .sh)"
  b2r:import "${B2R_MODULE}" "${B2R_FILE}"
  printf "\\e[1m⯈ Test %s::%s\\e[0m\\n" "${B2R_MODULE}" "${B2R_FILE}"

  # Launch SHUnit process
  # We must allow tests to fail before loading the library
  set +e
  # shellcheck disable=SC1090
  source "${SHUNIT_INIT_FILE}"
}

function search_shunit() {
  declare -ra SEARCH_PATHS=(
    shunit2
    /usr/local/share/shunit2/shunit2
    /usr/local/share/shunit2
    /usr/share/shunit2/shunit2
    /usr/share/shunit2
    ../shunit2/shunit2
    ../shunit2
  )

  if [[ -n "${SHUNIT_PATH:-}" ]] && [[ -f "${SHUNIT_PATH}" ]]; then
    echo "${SHUNIT_PATH}"
    return
  fi

  for TESTED_PATH in "${SEARCH_PATHS[@]}"; do
    if [[ -f "${TESTED_PATH}" ]]; then
      echo "${TESTED_PATH}"
      return
    fi
  done

  printf "\e[31;1mImpossible to find SHUnit.\e[0m\n" >&2
  exit 255
}

function assertError() {
  if [[ ${#} = 4 ]]; then
    declare -r TEST_MESSAGE="${1}"
    shift
  else
    declare -r TEST_MESSAGE="Test error"
  fi
  declare -r EXPECTED_CODE="${1}"
  declare -r EXPECTED_MESSAGE="${2}"
  declare -r CMD="${3}"
  # Temporary file: it will be cleaned on exit by SHUnit.
  declare -r TEST_STDERR="${SHUNIT_TMPDIR}/stderr"

  ${CMD} >/dev/null 2>|"${TEST_STDERR}"
  declare -ri CODE="${?}"

  assertEquals "${TEST_MESSAGE}: unexpected return code." "${EXPECTED_CODE}" "${CODE}"
  assertEquals "${TEST_MESSAGE}: unexpected error message." "${EXPECTED_MESSAGE}" "$(cat "${TEST_STDERR}")"
}

function assertErrorContains() {
  if [[ ${#} = 4 ]]; then
    declare -r TEST_MESSAGE="${1}"
    shift
  else
    declare -r TEST_MESSAGE="Test error"
  fi
  declare -r EXPECTED_CODE="${1}"
  declare -r EXPECTED_MESSAGE="${2}"
  declare -r CMD="${3}"
  # Temporary file: it will be cleaned on exit by SHUnit.
  declare -r TEST_STDERR="${SHUNIT_TMPDIR}/stderr"

  ${CMD} >/dev/null 2>|"${TEST_STDERR}"
  declare -ri CODE="${?}"

  assertEquals "${TEST_MESSAGE}: unexpected return code." "${EXPECTED_CODE}" "${CODE}"
  assertContains "${TEST_MESSAGE}: unexpected error message." "$(cat "${TEST_STDERR}")" "${EXPECTED_MESSAGE}"
}

# Launch tests at the end of script
trap launch_shunit EXIT
