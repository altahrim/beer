#!/usr/bin/env bash
# @file Test suite for array helpers
# shellcheck source=tests/init.sh
source "$(dirname "$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")")/init.sh"

# Setup
function oneTimeSetUp() {
  # shellcheck disable=SC2034
  declare -grA ASSOC_ARRAY=(
    [key]=value
    [empty]=""
    [other]=123
    ["my key"]="valid value"
  )
  # shellcheck disable=SC2034
  declare -gra EMPTY_ARRAY=()
}

function testKeyExists() {
  assertTrue "Function must return 0 if key exists." "b2r:array:key_exists key ASSOC_ARRAY"
  assertTrue "Function must return 0 if key exists, even if value is empty." "b2r:array:key_exists empty ASSOC_ARRAY"
  assertTrue "Function must return 0 if key exists, even if key is composed." "b2r:array:key_exists 'my key' ASSOC_ARRAY"
}

function testKeyDoesntExist() {
  assertFalse "Function must return >0 if key doesn’t exist." "b2r:array:key_exists nope ASSOC_ARRAY"
  assertFalse "Function must return >0 if key doesn’t exist, even if key is part of a composed key." "b2r:array:key_exists my ASSOC_ARRAY"
}

function testKeyExistsInEmptyArray() {
  assertError "Function must return 2 on empty array" 2 "" "b2r:array:key_exists key EMPTY_ARRAY"
}

function testKeyExistsInUndefinedArray() {
  assertError "Function must fail on undefined array" 3 "" "b2r:array:key_exists key NON_EXISTING_ARRAY"
}

function testValueExists() {
  assertTrue "Function must return 0 if value exists." "b2r:array:exists value ASSOC_ARRAY"
  assertTrue "Function must return 0 if value exists, even if value is composed." "b2r:array:exists 'valid value' ASSOC_ARRAY"
  assertTrue "Function must return 0 if value exists, even if value is empty." "b2r:array:exists '' ASSOC_ARRAY"
}

function testValueDoesntExist() {
  assertFalse "Function must return >0 if value doesn’t exist (1)." "b2r:array:exists 1234 ASSOC_ARRAY"
  assertFalse "Function must return >0 if value doesn’t exist (2)." "b2r:array:exists valid ASSOC_ARRAY"
}

function testValueExistsInEmptyArray() {
  assertError "Function must return 2 on empty array" 2 "" "b2r:array:exists key EMPTY_ARRAY"
}

function testValueExistsInUndefinedArray() {
  assertError "Function must fail on undefined array" 3 "" "b2r:array:exists key NON_EXISTING_ARRAY"
}


